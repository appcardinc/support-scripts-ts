# README #

### What is this repository for? ###

* simplify life of tech support
* script out repeated task

### Where are they located in prod? ###

* plprod-ts1
* /home/support/SCRTIPS

### How to execute

* You must be on plprod-ts1
* simply type the name of the script as if it were a command

```
-> jbs-get-connections.sh 
USAGE:
jbs-get-connections.sh jbrain
```

* follow what it says:

```
-> jbs-get-connections.sh tamu46668
### NTRs connected to the jbrain ###
tcp6       0      0 192.168.10.10:1883      192.168.10.12:51056     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.15:33096     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.16:50426     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.17:47629     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.19:59923     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.22:34655     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.25:34555     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.27:45890     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.28:35619     ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.3:34791      ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.4:39920      ESTABLISHED 19125/java          
tcp6       0      0 192.168.10.10:1883      192.168.10.5:58346      ESTABLISHED 19125/java          
tcp6       0      0 :::1883                 :::*                    LISTEN      19125/java          

### POSs connected to the jbrain ###
tcp6       0      0 :::47827                :::*                    LISTEN      19145/java          

### Printers connected to the jbrain ###
tcp6       0      0 :::27001                :::*                    LISTEN      19145/java          

### Reward Printers connected to the jbrain ###
```