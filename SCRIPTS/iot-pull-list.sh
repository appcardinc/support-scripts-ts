#!/bin/bash 

# script to get all IOT device info

# supported devices:
# - Phantom
# - Standard USB
# - PT7003-Terminal

trap 'rm -f /tmp/*-$$' EXIT

export PGHOST="prod-rtdb.c40ku8gr3zsv.us-east-1.rds.amazonaws.com"
export PGPORT="5432"
export PGDATABASE="AppCardDB"
export PGUSER="ntrsearch"
export PGPASSWORD="e9e49a5801ef7b16cfe05921e"

function create_iot_query () {
cat >/tmp/iot-query-$$ <<-EOF
select row_to_json(output) from (
SELECT
  d.device_id,
  d.device_name,
  d.ssh_port,
  d.setup_type,
  l.lane_number,
  d.branch_id,
  mb.name as branch_name,
  d.merchant_id,
  m.name as merchant_name
FROM devices d,
     lanes l,
     merchant_branches mb,
     merchants m
WHERE d.setup_type = '$setup_type'
  and d.lane_id = l.lane_id
  and l.lane_number is not null
  and d.branch_id = mb.branch_id
  and d.merchant_id = m.merchant_id
  and m.status <> 2
ORDER BY
  d.merchant_id,
  d.branch_id,
  d.device_name
) output;
EOF
}
function create_ntr_query() {
cat >/tmp/iot-query-$$ <<-EOF
select row_to_json(output) from (
SELECT DISTINCT ON (msn.serial_number)
  d.device_id,
  d.device_name,
  d.ssh_port,
  d.setup_type,
  msn.serial_number,
  l.lane_number,
  l.lane_id,
  d.branch_id,
  mb.name as branch_name,
  d.merchant_id,
  m.name as merchant_name,
  m.status
FROM devices d
     JOIN lanes l ON d.lane_id = l.lane_id and l.lane_number is not null
     JOIN merchant_branches mb ON d.branch_id = mb.branch_id
     JOIN branch_devices_serial_numbers msn ON d.device_id = msn.device_id
     JOIN merchants m ON d.merchant_id = m.merchant_id and m.status <> 2
WHERE d.setup_type = '$setup_type'
ORDER BY
  msn.serial_number,
  d.create_tstamp DESC,
  d.merchant_id,
  d.branch_id,
  d.device_name
) output;
EOF
}

function if_ntr_or_phantom () {
	#if [[ $dev == "ntrs" ]] || [[ $dev == "phantoms" ]]; then
	if [[ $dev == "phantoms" ]]; then
		sed -i '/d.setup_type,/a  msn.serial_number,' /tmp/iot-query-$$
		sed -i '/merchant_branches mb,/a     branch_devices_serial_numbers msn,' /tmp/iot-query-$$
		sed -i '/and d.merchant_id = m.merchant_id/a  and msn.device_id = d.device_id' /tmp/iot-query-$$
	fi
	return
	if [[ $dev == "ntrs" ]]; then
		sed -i '/d.setup_type,/a  ac.property_name,' /tmp/iot-query-$$
		sed -i '/d.setup_type,/a  ac.property_value,' /tmp/iot-query-$$
		sed -i '/merchant_branches mb,/a     app_configuration ac,' /tmp/iot-query-$$
		sed -i '/and d.merchant_id = m.merchant_id/a  and ac.device_id = d.device_id' /tmp/iot-query-$$
	fi
}

function get_data () {
	cat /tmp/iot-query-$$ | psql -t -AF ',' | grep -vE 'rows\)|row\)' >> /tmp/iot-list-$$
}

function setup_type () {
	var=$1
	case "$var" in
		ntrs)		setup_type="PT7003-Terminal";;
		otrs)		setup_type="Standard USB";;
		phantoms)	setup_type="Phantom";;
		all)		setup_type="all";;
	esac
}

# if run script without any parameters
if [ -z $1 ]; then
	PS3="Please select the platform: "
	select choose_platform in ntrs otrs phantoms all; do
		case "$choose_platform" in
			ntrs)		setup_type="PT7003-Terminal"; dev=$choose_platform; break;;
			otrs)		setup_type="Standard USB"; dev=$choose_platform; break;;
			phantoms)	setup_type="Phantom"; dev=$choose_platform; break;;
			all)		setup_type="all"; break;;
			*)		echo -e "\nPlease use the numbers.";;
		esac
	done
else
	# if run script with parameters; they must be specific
	if [[ $1 == ntrs ]] || [[ $1 == otrs ]] || [[ $1 == phantoms ]] || [[ $1 == all ]]; then
		dev=$1
		setup_type $1
		choose_platform=$1
	else
		echo Error exiting
		exit 1
	fi
fi

# if all then loop through all possible setup_type(s)
if [[ $setup_type == "all" ]];	then
	for i in ntrs otrs phantoms; do
		dev=$i
		setup_type $i
		if [[ $dev == "ntrs" ]];	then
			create_ntr_query
		else
			create_iot_query
		fi
		if_ntr_or_phantom
		get_data
	done
	choose_platform=iots
else # else query a specific setup_type
	if [[ $dev == "ntrs" ]];	then
		create_ntr_query
	else
		create_iot_query
	fi
	if_ntr_or_phantom
	get_data
fi

# show the output
cat /tmp/iot-list-$$ | jq -s -c 'sort_by(.merchant_id, .branch_id, .lane_number) | .[]'

