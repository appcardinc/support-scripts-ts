#!/bin/bash
#===============================================================================
#
#          FILE: jbs-pull-list.sh
# 
#         USAGE: ./jbs-pull-list.sh
# 
#   DESCRIPTION: This script queries the Oracle DB and gets a list of all JBs
#                in the field and outputs it in a csv file
#       OPTIONS: ---
#  REQUIREMENTS: ---
#          BUGS: ---
#	  NOTES: ---
#        AUTHOR: ---
#  ORGANIZATION: AppCard Inc
#       CREATED: 03/17/2021 17:30
#       UPDATED: ---
#===============================================================================
## how to access oracle db from CLI
# sqlplus64 -S username/mypassword@'prodrotw.c40ku8gr3zsv.us-east-1.rds.amazonaws.com:1521/PRODROTW'  
## basic query being done
# select NAME,SHORT_NAME,STORE_ID,STORE_NUMBER,SERVER_IP,OWNER_ID,CLIENT_ID,ADDRESS_ID FROM  common.store WHERE server_ip != '0.0.0.0' AND jbrain_type != 4 AND jbrain_type != 3 AND deleted is NULL
## long linux one liner from the CLI
# echo -e "select SHORT_NAME,SERVER_IP,STORE_NUMBER,NAME FROM common.store WHERE server_ip != '0.0.0.0' AND jbrain_type != 4 AND jbrain_type != 3 AND deleted is NULL" | sqlplus64 -S username/password@'prodrotw.c40ku8gr3zsv.us-east-1.rds.amazonaws.com:1521/PRODROTW'  
#===============================================================================

trap 'rm /tmp/*-$$ 2> /dev/null' EXIT

#####################################################################
# sql user creds and sql details
SQL_USER='jbsearch'
SQL_PASS=$(echo $SQL_USER | md5sum | head -c 25)
SQL_HOST="prodrotw.c40ku8gr3zsv.us-east-1.rds.amazonaws.com:1521"
SQL_HOST2="prodrow2.c40ku8gr3zsv.us-east-1.rds.amazonaws.com:1521"
SQL_DB="PRODROTW"
SQL_DB2="PRODROW2"
#####################################################################

function output_format_echo () {
	echo -e "Output format:"
	echo -e "SHORT_NAME,SERVER_IP,STORE_NUMBER,NAME"
}

# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "$(basename $0) jbname (pull by name or part of the name)"
	echo "$(basename $0) all (all means pull all jbs)"
	echo
	echo "example:"
	echo "$(basename $0) ft0541"
	echo "$(basename $0) ft"
	echo
	output_format_echo
	exit 1
fi

# change 1st parameter input to uppercase
JB_NAME=$(echo $1 | tr '[:lower:]' '[:upper:]')

# create the sql query; note it is not complete. there is no ';' 
function create_query () {
	#NOTE 
	# jbrain_type = 1 equals prod jbs
	# jbrain_type = 2 equals spare jbs
	# jbrain_type = 3 equals lab jbs
	# jbrain_type = 4 equals superjbs
	cat >/tmp/dummy-$$ <<-EOF
	set feedback off
	set colsep ,
	set headsep off
	set pagesize 0
	set trimspool on
	set linesize 1000
	set WRAP off
	select REPLACE(SHORT_NAME, ' ', ''),SERVER_IP,STORE_NUMBER,REPLACE(NAME, ',', '')
	FROM common.store
	WHERE deleted is NULL AND server_ip != '0.0.0.0'
	AND jbrain_type != 3
	AND jbrain_type != 4
	EOF
}

create_query

# at this point add whatever to the end of the query
if [[ $JB_NAME == ALL ]]; then
	WANT_ALL=1
elif [[ $JB_NAME == AUDIT ]]; then # used for auditing for JBs that are alive but have no IP in the db
	sed -i 's/server_ip !=/server_ip =/g' /tmp/dummy-$$
	WANT_ALL=1
elif [[ $JB_NAME == SUPER ]]; then # get superJs
	sed -i 's/!= 4/= 4/g' /tmp/dummy-$$
	WANT_ALL=1
else
	cat >>/tmp/dummy-$$ <<-EOF
	AND SHORT_NAME like '%$JB_NAME%'
	EOF
	WANT_ALL=0
fi

echo ";" >> /tmp/dummy-$$

# query db to pull list in csv format
cat /tmp/dummy-$$ | sqlplus64 -S $SQL_USER/$SQL_PASS@"$SQL_HOST/$SQL_DB" > /tmp/jbs-master-list-$$
#cat /tmp/dummy-$$ | sqlplus64 -S $SQL_USER/$SQL_PASS@"$SQL_HOST2/$SQL_DB2" >> /tmp/jbs-master-list-$$

# format data
cat /tmp/jbs-master-list-$$ | sed 's/  //g; s/\t //g; s/ \t//g; s/\t//g' > /tmp/jbs-master-list-formated-$$

# if user wants a specific JB
if [ "$WANT_ALL" -eq 0 ]; then
	#output_format_echo
	sort /tmp/jbs-master-list-formated-$$
	exit 0
fi

# cp temp file to HOME dir of user
sort /tmp/jbs-master-list-formated-$$ 

