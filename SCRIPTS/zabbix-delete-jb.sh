#!/bin/bash

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT


# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "./script jbname"
	echo
	exit 1
fi

# change 1st parameter input to uppercase
JB_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')

zabbix_user="zabbix-api-user"
zabbix_pass=$(echo zabbix-api-user | md5sum | head -c 25)

if host $JB_NAME | grep -iq "has address" ; then
	JB_IP=$(host $JB_NAME | grep -i address | awk '{print $NF}')
	if host $JB_NAME | grep -q "cpe.greenpoints.com"; then # then regular JB
		JB_FQDN=$(echo $JB_NAME | sed 's/$/.cpe.greenpoints.com/g')
	elif host $JB_NAME | grep -q "pl.appcard.com"; then # then super JB
		JB_FQDN=$(echo $JB_NAME | sed 's/$/.appcard.com/g')
	else
		echo "Not sure what type of JB this is...either regular or SuperJB"
		exit 1
	fi
else
	echo Looks like $JB_NAME is not in dns. Please add
	exit 1
fi

cat >/tmp/$USER-zabbix-creds-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "$zabbix_user",
        "password": "$zabbix_pass"
    },
    "id": 1
}
EOF

USER_AUTH_COOKIE=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-creds-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result')


cat >/tmp/$USER-zabbix-get-host-id-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
	"output": ["hostid"],
        "filter": {
            "host": [
                "$JB_FQDN"
            ]
        }
    },
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF

curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-id-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null > /tmp/$USER-zabbix-output-$$

# get only fields: host,interfaces.ip,groups[].name
JB_HOST_ID=$(jq -r -c '.result[].hostid' /tmp/$USER-zabbix-output-$$)

cat >/tmp/$USER-zabbix-delete-host-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.delete",
    "params": ["$JB_HOST_ID"],
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF

curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-delete-host-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null
