#!/bin/bash

# some stores do not allow snmp queries
# so we use ssh to call a script called /etc/snmp/getEverything

# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "./script host (host is jbname or IP)"
	echo "./script file (file contains list of hosts)"
	echo
	exit 1
fi


trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT

function ssh_get_info () {
	if ping -c 2 $HOST &> /dev/null; then
		/usr/bin/ssh -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $HOST "/etc/snmp/getEverything" > /tmp/output-$$
	fi
}

function compile_data () {
	if [ -e /tmp/output-$$ ]; then
		echo "$HOST,$(cat /tmp/output-$$)" >> /tmp/ssh-output-$$
		rm -f /tmp/output-$$
	else
		echo "$HOST,,,," >> /tmp/ssh-output-$$
	fi
}

if [ -e $1 ]; then
	for i in $(cat $1); do
		HOST=$i
		ssh_get_info
		compile_data
	done
else	
	HOST=$1
	ssh_get_info
	compile_data
fi


sort /tmp/ssh-output-$$
