#!/bin/bash

# the rundeck job "See device connections" depends on this script
# you can also run this script locally

trap 'rm -f /tmp/*-$$' EXIT

if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "$(basename $0) jbrain"
	exit 0
fi

shopt -s expand_aliases
alias myssh="/usr/bin/ssh -l root  -o 'UserKnownHostsFile=/dev/null' -o StrictHostKeyChecking=no -o ServerAliveInterval=20"
alias myscp="scp -p -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=10 -o ServerAliveInterval=3"
today_date=$(date '+%Y-%m-%d')
jbrain=$1

# jbrain not in dns
if ! host $jbrain &> /dev/null; then
	echo Sorry, $jbrain is not in dns or wrong name
	exit 1
fi

# jbrain cannot ssh
if ! myssh $jbrain "hostname" &> /dev/null; then
	echo Sorry, cannot ssh into $jbrain
	exit 1
fi

echo "### NTRs connected to the jbrain ###"
p=1883 # ntr port
myssh $jbrain "netstat -nap | grep -E --color=auto ':$p' | sort -k 5" 2> /dev/null
echo

echo "### POSs connected to the jbrain ###"
p="47827|:8224" # pos port
myssh $jbrain "netstat -nap | grep -E --color=auto ':$p' | sort -k 5" 2> /dev/null
echo

echo "### Printers connected to the jbrain ###"
p=27001 # printer port
myssh $jbrain "netstat -nap | grep -E --color=auto ':$p' | sort -k 5" 2> /dev/null
echo

echo "### Reward Printers connected to the jbrain ###"
p=9100 # reward printer
myssh $jbrain "netstat -nap | grep -E --color=auto ':$p' | sort -k 5" 2> /dev/null
echo

