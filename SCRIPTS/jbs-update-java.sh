#!/bin/bash

if [ "$#" -eq 0 ]; then
	echo Please add a JB as a parameter
	echo "$(basename $0) JB"
	echo 
	exit 1
fi

HOST=$1

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT


cat <<-EOL
Purpose of this script is to:
	* update java to jdk1.8.0_202
	* add/update some parameters to engineConfiguration.xml (which you will do manually)

EOL

read -p "Press Enter to conitnue..."

echo -e "\n\n"


###############################################################
echo "Checking if java8 folder exists"
ssh -l root $HOST "ls -d /usr/java/jdk1.8.0_202" | grep jdk1.8.0_202 > /tmp/java-found-$$

if grep -q jdk1.8.0_202 /tmp/java-found-$$; then
	echo "Java8 folder found...continuing"
	echo -e "\n\n"
	sleep 2
else
	# try again
	echo "Java8 folder NOT found...will attempt to add"
	scp /home/techs/java/jdk1.8.0_202.tar.gz root@$HOST:
	ssh -l root $HOST "tar xfP jdk1.8.0_202.tar.gz ; rm -f jdk1.8.0_202.tar.gz"
	echo "REchecking if java8 folder exists"
	ssh -l root $HOST "ls -d /usr/java/jdk1.8.0_202" | grep jdk1.8.0_202 > /tmp/java-found-$$

	if grep -q jdk1.8.0_202 /tmp/java-found-$$; then
		echo "Java8 folder found...continuing"
		echo -e "\n\n"
		sleep 2
	else
		cat <<-EOL
		Please scp /home/techs/java/jdk1.8.0_202.tar.gz to this JB $HOST
		Then run:
		tar Pxf jdk1.8.0_202.tar.gz
		rm -f jdk1.8.0_202.tar.gz
		exiting...
		EOL
		sleep 1
		exit 1
	fi
fi

###############################################################
echo "Recreating symlink"
ssh -l root $HOST "rm -f /usr/java/latest; ln -s /usr/java/jdk1.8.0_202 /usr/java/latest"
echo -e "\n\n"

###############################################################
echo "Checking if java was updated"
ssh -l root $HOST "java -version" &> /tmp/java-found-$$
echo -e "\n\n"

if grep -q 1.8.0_202 /tmp/java-found-$$; then
	echo "java on $HOST was updated to 1.8.0_202...continuing"
	echo -e "\n\n"
	sleep 1
else
	cat <<-EOL
	Looks like java was not updated...
	Please make sure /usr/java/latest is pointing to /usr/java/jdk1.8.0_202 folder
	ls -l /usr/java/latest
	exiting...
	EOL
	sleep 1
	exit 1
fi

echo -e "\ntomcat has now been updated on $HOST"
echo -e "tomcat was not restarted\n"
exit

###############################################################
echo "Restarting tomcat"
ssh -l root $HOST "service tomcat restart" 
ssh -l root $HOST "service tomcat status" 

echo -e "\n\n"
echo "Script finished for $HOST"
echo "Verify"
