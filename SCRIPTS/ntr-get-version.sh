#!/bin/bash 

# script to get all ntr version

# output is recorded here:
# https://docs.google.com/spreadsheets/d/1pKDVc87G6x056LBUj397JNIStrhQ647XEY1264XPKBQ/edit#gid=0

export PGHOST="prod-rtdb.c40ku8gr3zsv.us-east-1.rds.amazonaws.com"
export PGPORT="5432"
export PGDATABASE="AppCardDB"
export PGUSER="ntrsearch"
export PGPASSWORD="e9e49a5801ef7b16cfe05921e"
export AWS_CONFIG_FILE=/home/support/.aws/config
export AWS_SHARED_CREDENTIALS_FILE=/home/support/.aws/credentials

trap 'rm -f /tmp/*-$$' EXIT

# get ntr restarts by branch
cat >/tmp/get-ntr-details-$$ <<-EOF
SELECT DISTINCT ON (msn.serial_number)
  d.device_id,
  d.setup_type,
  msn.serial_number,
  d.merchant_id,
  replace(m.name, ',', '') as merchant_name,
  d.branch_id,
  replace(mb.name, ',', '') as branch_name
FROM devices d
     JOIN lanes l ON d.lane_id = l.lane_id and l.lane_number is not null
     JOIN merchant_branches mb ON d.branch_id = mb.branch_id
     JOIN branch_devices_serial_numbers msn ON d.device_id = msn.device_id
     JOIN merchants m ON d.merchant_id = m.merchant_id and m.status <> 2 and m.category = 'Grocery'
WHERE d.setup_type = 'PT7003-Terminal'
  AND d.status != 3
ORDER BY
  msn.serial_number,
  d.create_tstamp DESC,
  d.merchant_id,
  d.branch_id
;
EOF

# run the sql cmd on postgress; remove the summary line; 
cat /tmp/get-ntr-details-$$ | psql -AF ',' | grep -vE 'rows\)|row\)' > /tmp/sql-output-$$

# get only ntrs
cut -d, -f1 /tmp/sql-output-$$ | grep -v device_id > /tmp/ntr-device-ids-$$
cp /tmp/ntr-device-ids-$$ $(pwd)/file.txt

## start of python code and output to file
python - <<EOF > /tmp/ntr-device-data-$$
import boto3
import json

AWS_REGION = 'us-east-1'

def load_aws_credentials():
    session = boto3.Session()
    credentials = session.get_credentials()
    return credentials


credentials = load_aws_credentials()
client = boto3.client('iot-data', region_name=AWS_REGION, aws_access_key_id=credentials.access_key,
                      aws_secret_access_key=credentials.secret_key, aws_session_token=credentials.token)


with open("/tmp/ntr-device-ids-$$", "r") as input_file:
  for line in input_file:
    device = line.strip()
    data = json.loads(client.get_thing_shadow(thingName=str(device))['payload'].read())
    try:
        print(data['state']['desired']['firmware']['version'] + ',' + data['state']['reported']['current']['version'] + ',' + data['state']['reported']['current']['firmware'])
    except KeyError:
        print("Corrupted config on device ID" + ',' + device)
        #print("Corrupted config on device ID, {device}")
EOF

# add the header and add a comman at the beginning
echo -e "Desired Version,NTR version,FW version\n$(cat /tmp/ntr-device-data-$$)" | sed 's/^/,/g' > /tmp/ntr-final-$$

# print both csv formated data side by side and remove any <TAB>
pr -m -t -s /tmp/sql-output-$$ /tmp/ntr-final-$$ | sed 's/\t//g'
