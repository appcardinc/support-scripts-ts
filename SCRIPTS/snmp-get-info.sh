#!/bin/bash

# reference:
# https://runops.wordpress.com/2015/04/04/create-custom-snmp-oid-in-linux/

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT

# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "./script host (host is jbname or IP)"
	echo "./script file (file contains list of hosts)"
	echo
	exit 1
fi


function compile_data () {
	# output data
	echo "$HOST,$HOST_NAME,$IPS,$NTRcount,$OS_RELEASE" >> /tmp/snmp-output-$$
}
function snmp_get_info () {
	# get OS release
	snmpwalk -v2c -c public $HOST .1.3.6.1.4.1.8072.1.3.2.4.1.2.12.103.101.116.79.83.82.101.108.101.97.115.101 2> /dev/null > /tmp/$HOST-snmp-output-$$
	if [ -f /tmp/$HOST-snmp-output-$$ ]; then
		OS_RELEASE=$(cat /tmp/$HOST-snmp-output-$$ | cut -d'=' -f2 | cut -d: -f2 | sed 's/^ //g')
	else
		OS_RELEASE=""
	fi
	snmpwalk -v2c -c public $HOST sysName 2> /dev/null > /tmp/$HOST-snmp-output-$$
	if [ -f /tmp/$HOST-snmp-output-$$ ]; then
		HOST_NAME=$(cat /tmp/$HOST-snmp-output-$$ | cut -d'=' -f2 | cut -d: -f2 | cut -d. -f1 | sed 's/^ //g')
	else
		HOST_NAME=""
	fi
	# get IPs
	snmpwalk -v2c -c public $HOST .1.3.6.1.4.1.8072.1.3.2.3.1.1.6.103.101.116.73.80.115 2> /dev/null > /tmp/$HOST-snmp-output-$$
	if [ -f /tmp/$HOST-snmp-output-$$ ]; then
		IPS=$(cat /tmp/$HOST-snmp-output-$$ | cut -d'=' -f2 | awk '{print $2}')
	else
		IPS=""
	fi
	# get ntrcount
	snmpwalk -v2c -c public $HOST .1.3.6.1.4.1.8072.1.3.2.4.1.2.11.103.101.116.78.84.82.99.111.117.110.116.1 2> /dev/null > /tmp/$HOST-snmp-output-$$
	if [ -f /tmp/$HOST-snmp-output-$$ ]; then
		NTRcount=$(cat /tmp/$HOST-snmp-output-$$ | cut -d'=' -f2 | awk '{print $2}')
	else
		NTRcount=""
	fi
}



if [ -e $1 ]; then
	for i in $(cat $1); do
		HOST=$i
		snmp_get_info
		compile_data
	done
else	
	HOST=$1
	snmp_get_info
	compile_data
fi

sort /tmp/snmp-output-$$
