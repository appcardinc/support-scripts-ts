#!/bin/bash

# the rundeck job "Get Transaction Logs from jbrain" depends on this script
# you can also run this script locally

trap 'rm -f /tmp/*-$$' EXIT

if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "$(basename $0) jbrain lane_id trn_id date"
	echo "Ex:"
	echo "$(basename $0) TSRV0312 3 104596 2021-08-16"
	exit 0
fi

shopt -s expand_aliases
alias myssh="/usr/bin/ssh -l root  -o 'UserKnownHostsFile=/dev/null' -o StrictHostKeyChecking=no -o ServerAliveInterval=20"
alias myscp="scp -p -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=10 -o ServerAliveInterval=3"
today_date=$(date '+%Y-%m-%d')
jbrain=$1
lane=$2
trnid=$3
date=$4

# if someone wants to put a future date; exit
if [ $(date -d "$date" +%s) -gt $(date -d "$today_date" +%s) ]; then
	echo "Sorry, $date is in the future...exiting"
	exit 1
fi

# jbrain not in dns
if ! host $jbrain &> /dev/null; then
	echo Sorry, $jbrain is not in dns or wrong name
	exit 1
fi

# jbrain cannot ssh
if ! myssh $jbrain "hostname" &> /dev/null; then
	echo Sorry, cannot ssh into $jbrain
	exit 1
fi

# sed all lines from $date to end of output;
# need to show all the logs in ascending order: $(ls bla/bla.log* | tac)
myssh $jbrain "sed -n '/^$date/,\$p' \$(ls /var/log/tomcat/posmessages.log* | tac) | grep -avE ' RUUP| IMUP'" > /tmp/$jbrain-posmessages-log-$$
myssh $jbrain "sed -n '/^$date/,\$p' \$(ls /var/log/tomcat/engine.log* | tac) | grep -avE ' RUUP| IMUP'" > /tmp/$jbrain-engine-log-$$

# if date is not today then it must be the past... so create a future date of $date; obvioulsy $date + 1 day
if ! [[ "$date" == "$today_date" ]]; then
	date_future=$(date -d "$date+1 day" '+%Y-%m-%d')
	# delete any lines that have a date future to $date
	sed -i "/^$date_future .*$/,\$d" /tmp/$jbrain-posmessages-log-$$
	sed -i "/^$date_future .*$/,\$d" /tmp/$jbrain-engine-log-$$
fi

# get branch specific variables
jbrain_header=$(myssh $jbrain "grep -E '>POS_MESSAGE_HEADER_SIZE<|<value>' /opt/shsolutions/conf/engineConfiguration.xml | grep -A 1 '>POS_MESSAGE_HEADER_SIZE<' | grep value | cut -d'>' -f2 | cut -d'<' -f1")
retid=$(myssh $jbrain "grep retailerId /opt/shsolutions/conf/storeConfiguration.xml | cut -d'>' -f2 | cut -d'<' -f1")
storeid_raw=$(myssh $jbrain "grep storeId /opt/shsolutions/conf/storeConfiguration.xml | cut -d'>' -f2 | cut -d'<' -f1")

# validate what search string to look for
if [ $jbrain_header -eq 10 ]; then
	SEARCH_RECORD_STRING=" A 0 0 "
elif [ $jbrain_header -eq 6 ]; then
	SEARCH_RECORD_STRING=" B 0 0 "
else
	echo "Looks like the header, $jbrain_header , is not 6 or 10"
	exit 1
fi

# if trnid number >= to 4 then only give me the last 4 digits
if [ $(echo ${#trnid}) -ge 4 ]; then
	truncated_trnid=${trnid: -4}
	# if there is a leading 0; then remove
	if echo $truncated_trnid | grep -q "^0"; then
		truncated_trnid=$(echo $truncated_trnid | sed 's/^0//g')
	fi
else
	truncated_trnid=$trnid
fi


# try to find the trn logs
if grep -q -a " $lane $truncated_trnid " /tmp/$jbrain-posmessages-log-$$; then
	grep -a " $lane $truncated_trnid " /tmp/$jbrain-posmessages-log-$$ | grep "$SEARCH_RECORD_STRING" | head -1 > /tmp/$jbrain-trnid-data-$$
	grep -a " $lane $truncated_trnid " /tmp/$jbrain-engine-log-$$ > /tmp/$jbrain-trnid-logs-$$
	grep -a " $lane $truncated_trnid " /tmp/$jbrain-engine-log-$$ | grep -a " E 0 0 " > /tmp/$jbrain-wholetrnid-logs-$$

elif grep -q -a " $lane  $truncated_trnid " /tmp/$jbrain-posmessages-log-$$; then
	grep -a " $lane  $truncated_trnid " /tmp/$jbrain-posmessages-log-$$ | grep "$SEARCH_RECORD_STRING" | head -1 > /tmp/$jbrain-trnid-data-$$
	grep -a " $lane  $truncated_trnid " /tmp/$jbrain-engine-log-$$ > /tmp/$jbrain-trnid-logs-$$
	grep -a " $lane  $truncated_trnid " /tmp/$jbrain-engine-log-$$ | grep -a " E 0 0 " > /tmp/$jbrain-wholetrnid-logs-$$

elif grep -q -a " $lane   $truncated_trnid " /tmp/$jbrain-posmessages-log-$$; then
	grep -a " $lane   $truncated_trnid " /tmp/$jbrain-posmessages-log-$$ | grep "$SEARCH_RECORD_STRING" | head -1 > /tmp/$jbrain-trnid-data-$$
	grep -a " $lane   $truncated_trnid " /tmp/$jbrain-engine-log-$$ > /tmp/$jbrain-trnid-logs-$$
	grep -a " $lane   $truncated_trnid " /tmp/$jbrain-engine-log-$$ | grep -a " E 0 0 " > /tmp/$jbrain-wholetrnid-logs-$$
else
	echo "I could not find logs for: TRN: $trnid , LANE: $lane , DATE: $date"
	exit 1
fi

# get vars for transaction summary
lid=$(cat /tmp/$jbrain-trnid-data-$$ | awk '{print $15}')
delta_trnid=$(cat /tmp/$jbrain-wholetrnid-logs-$$ | awk '{print $20}')

# remove all leading zeros (0) from storeid
storeid=$storeid_raw
while true; do
	if echo $storeid | grep -q "^0"; then
		storeid=$(echo $storeid | sed 's/^0//g')
	else
		break
	fi
done
		
# output the logs
cat /tmp/$jbrain-trnid-logs-$$ | grep -av "POS Printer"

# output the summary
if grep -a -q "^\-\- Trn # $trnid ret_id $retid store_id $storeid reg_id $lane" /tmp/$jbrain-engine-log-$$; then
	
	sed -n "/^-- Trn # $trnid ret_id $retid store_id $storeid reg_id $lane /, /^-- Amount /p" /tmp/$jbrain-engine-log-$$
else
	sed -n "/^-- Trn # $delta_trnid ret_id $retid store_id $storeid reg_id $lane /, /^-- Amount /p" /tmp/$jbrain-engine-log-$$
fi
