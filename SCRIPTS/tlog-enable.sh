#!/bin/bash

# simple script to enable tlog

# tlog server is plprod-ng-tlog1

trap 'rm -f /tmp/*-$$' EXIT

if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "$(basename $0) <client_id>"
	echo Ex:
	echo "$(basename $0) 1234"
	echo 
	exit 1
fi

client_ID=$1

date_format=$(date -u "+%a, %d %b %Y %H:%m:%S GMT")

cat >/tmp/tlog-post-data.xml-$$ <<EOF
<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:hs="http://ws.broker.stlog.shsolutions.com/1">
<soapenv:Body>
   <hs:setAwsChannel>
      <TLogSetAwsChannelRequest>
         <active>true</active>
         <channelFormat>xml</channelFormat>
         <channelType>sqs</channelType>
         <destination>prod-tlog-queue.fifo</destination>
         <entityId>$client_ID</entityId>
      </TLogSetAwsChannelRequest>
   </hs:setAwsChannel>
</soapenv:Body>
</soapenv:Envelope>

EOF

curl -X POST http://172.10.21.135:8080/tlog-ws/tlogservice -H "content-type: text/xml;charset=utf-8 date: $date_format transfer-encoding: chunked" --data @/tmp/tlog-post-data.xml-$$ 2> /dev/null > /tmp/tlog-enable-output.xml-$$ 

xmllint --format /tmp/tlog-enable-output.xml-$$ 
