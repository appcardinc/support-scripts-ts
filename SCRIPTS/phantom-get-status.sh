#!/bin/bash

# validate user input
if [ "$#" -eq 0 ]; then
	echo -e "USAGE:"
	echo "$(basename $0) deviceID"
	echo "ex:"
	echo "$(basename $0) 12345"
	exit 1
fi

device_id=$1

USER_AUTH_COOKIE=$(curl -i -X POST -H "Content-Type:application/json" -d '{"username": "techsupport@appcard.com","password": "tL!022xd8VAn"}' 'https://appcard.com/api/users/token' -v -s 2> /dev/null | grep Set-Cookie: | cut -d';' -f1 | cut -d':' -f2 | sed 's/ //g; s/^/Cookie:/g')

curl -X GET -H "Accept: application/json" -H "$USER_AUTH_COOKIE;" "https://appcard.com/baseapi/1.0/support/check_device/$device_id" -v -s 2> /dev/null | jq

