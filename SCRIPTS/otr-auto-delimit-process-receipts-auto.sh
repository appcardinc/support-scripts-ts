#!/bin/bash

# validate user input
if [ "$#" -eq 0 ]; then
	echo -e "USAGE:"
	echo "$(basename $0) deviceID"
	echo "ex:"
	echo "$(basename $0) 12345"
	exit 1
fi

#function runs when script's process receives signal
function clean_up () {
	kill $ROTATE_PID 2> /dev/null; wait $ROTATE_PID 2> /dev/null
}       # ----------- end of function clean_up -----------

trap 'echo; clean_up' EXIT

device_id=$1

function timer () {
	num=$NUM
	while true; do
		size=${#num}
		case $size in
			"3")	echo -e " $num""\b\b\b\b\c"; sleep 1;;
			"2")	echo -e " $num ""\b\b\b\b\c"; sleep 1;;
			"1")	echo -e " $num  ""\b\b\b\b\c"; sleep 1;;
		esac
		num=`expr $num - 1`
	done
}

USER_AUTH_COOKIE=$(curl -i -X POST -H "Content-Type:application/json" -d "{\"username\": \"techsupport@appcard.com\",\"password\": \"tL!022xd8VAn\"}" 'https://appcard.com/api/users/token' -v -s 2> /dev/null | grep Set-Cookie: | cut -d';' -f1 | cut -d':' -f2 | sed 's/ //g; s/^/Cookie:/g')

echo -e "Executing: Release Operations > Auto Delimit\n"
curl -X POST -H "Accept: application/json" -H "$USER_AUTH_COOKIE;" "https://appcard.com/api/ng-capture-data/$device_id/auto-delimit" -v -s 2> /dev/null
echo

NUM=60
timer &
ROTATE_PID=$!	# Capture PID of last background process
echo -e "Please wait...  \b\c"; sleep $NUM; echo
kill $ROTATE_PID; wait $ROTATE_PID 2> /dev/null
echo

echo -e "Executing: Release Operations > Auto Process-Receipts\n"
curl -X POST -H "Accept: application/json" -H "$USER_AUTH_COOKIE;" "https://appcard.com/api/ng-capture-data/$device_id/auto-process-receipts" -v -s 2> /dev/null
