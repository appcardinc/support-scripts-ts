#!/bin/bash

# disable/enable ntr logs
# then pull them

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT

export AWS_CONFIG_FILE=/home/support/.aws/config
export AWS_SHARED_CREDENTIALS_FILE=/home/support/.aws/credentials

# validate user input
if [ "$#" -eq 0 ]; then
	echo -e "USAGE:"
	echo "$(basename $0) deviceID"
	echo "ex:"
	echo "$(basename $0) 12345"
	exit 1
fi

device_id=$1

# disable log on X device: see payload
echo "disabling logs on $device_id"
aws iot-data update-thing-shadow --thing-name $device_id --payload '{"state": {"desired":{"log_request": false}}}' /tmp/tmp.log-$$
echo done

sleep 2

# enable log on X device: see payload
echo "enabling logs on $device_id"
aws iot-data update-thing-shadow --thing-name $device_id --payload '{"state": {"desired":{"log_request": true}}}' /tmp/tmp.log-$$
echo done

echo "pulling logs of $device_id from s3"
aws s3 cp s3://android-terminal-logs/prod/$device_id/ ~/android_logs/$device_id --recursive
echo done

echo "unzipping logs from ~/android_logs/$device_id"
for z in ~/android_logs/$device_id/`date +"%Y-%m-%d"`-*.zip; do 
	unzip -o "$z" -d ~/android_logs/$device_id/
done

# delete zip files
rm -rf ~/android_logs/$device_id/*.zip
echo done

echo
