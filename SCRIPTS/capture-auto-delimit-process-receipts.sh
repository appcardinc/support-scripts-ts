#!/bin/bash

# script to run regex tool process. Specifically:
# - auto delimit
# - auto process receipts

# works with otrs and phantoms
# device id is required

# validate user input
if [ "$#" -eq 0 ]; then
	echo -e "USAGE:"
	echo "$(basename $0) deviceID"
	echo "ex:"
	echo "$(basename $0) 12345"
	exit 1
fi

device_id=$1

USER_AUTH_COOKIE=$(curl -i -X POST -H "Content-Type:application/json" -d "{\"username\": \"techsupport@appcard.com\",\"password\": \"tL!022xd8VAn\"}" 'https://appcard.com/api/users/token' -v -s 2> /dev/null | grep Set-Cookie: | cut -d';' -f1 | cut -d':' -f2 | sed 's/ //g; s/^/Cookie:/g')

function auto-delimit () {
	echo -e "Executing: Release Operations > Auto Delimit\n"
	curl -X POST -H "Accept: application/json" -H "$USER_AUTH_COOKIE;" "https://appcard.com/api/ng-capture-data/$device_id/auto-delimit" -v -s 2> /dev/null
}

function auto-process-receipts () {
	echo -e "Executing: Release Operations > Auto Process-Receipts\n"
	curl -X POST -H "Accept: application/json" -H "$USER_AUTH_COOKIE;" "https://appcard.com/api/ng-capture-data/$device_id/auto-process-receipts" -v -s 2> /dev/null
}

PS3="Please select what you want to do: "
select my_choice in "auto-delimit" "auto-process-receipts"; do
	case "$my_choice" in
		"auto-delimit")			echo; auto-delimit; break;;
		"auto-process-receipts")	echo; auto-process-receipts; break;;
		*)				echo -e "\nPlease use the numbers. Try again.\n";;
	esac
done
