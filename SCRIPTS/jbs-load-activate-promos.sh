#!/bin/bash

# script to load/activate promos on a jb.
# this script runs another script living on the jb

if [ $# -eq 0 ] ; then
	echo USAGE:
        echo "$(basename $0) lfjb0275 lfjb0278 ssjb1147"
        echo "Please try again and add a list of jbrains to load/activate promotions for..."
	echo "NOTE: you will need to have root access to the JBs"
        exit 1
fi

STORELIST=($@)

for STORE in "${STORELIST[@]}";
do
ping -c 1 $STORE > /dev/null 2>/dev/null
pingRet=$?
    if [ $pingRet -eq 0 ]; then
        echo ---------------------
        echo $STORE
	
	/usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $STORE "/home/support/pmt_loader/bin/load_all.sh enginewebservices enginewebservices"
    else
        echo ---------------------
        echo $STORE
        echo DOWN
    fi
done
echo ---------------------
