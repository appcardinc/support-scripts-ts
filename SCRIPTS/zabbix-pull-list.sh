#!/bin/bash

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT


# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "./script jbname"
	echo "./script all"
	echo
	exit 1
fi

# change 1st parameter input to uppercase
JB_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]')

zabbix_user="zabbix-api-user"
zabbix_pass=$(echo zabbix-api-user | md5sum | head -c 25)

if ! [[ $JB_NAME == all ]]; then
	if host $JB_NAME | grep -iq "has address" ; then
		JB_IP=$(host $JB_NAME | grep -i address | awk '{print $NF}')
		if host $JB_NAME | grep -q "cpe.greenpoints.com"; then # then regular JB
			JB_FQDN=$(echo $JB_NAME | sed 's/$/.cpe.greenpoints.com/g')
		elif host $JB_NAME | grep -q "pl.appcard.com"; then # then super JB
			JB_FQDN=$(echo $JB_NAME | sed 's/$/.appcard.com/g')
		else
			echo "Not sure what type of JB this is...either regular or SuperJB"
			exit 1
		fi
	else
		echo Looks like $JB_NAME is not in dns
		exit 1
	fi
fi

cat >/tmp/$USER-zabbix-creds-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "$zabbix_user",
        "password": "$zabbix_pass"
    },
    "id": 1
}
EOF

USER_AUTH_COOKIE=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-creds-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result')

function get_zbx_host () {
cat >/tmp/$USER-zabbix-get-host-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": "extend",
        "selectGroups": "extend",
        "selectInterfaces": "extend",
        "filter": {
            "host": [
                "$JB_FQDN"
            ]
        }
    },
    "auth": "$USER_AUTH_COOKIE",
    "id": 1
}
EOF
}

function get_zbx_all () {
cat >/tmp/$USER-zabbix-get-host-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["host"],
        "selectGroups": "extend",
        "selectInterfaces": ["ip"]
    },
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF
}

function create_json_file () {
	if [[ $JB_NAME == all ]]; then
		get_zbx_all
	else
		get_zbx_host
	fi
}
function output_info () {
	if [[ $JB_NAME == all ]]; then
	# get only fields: host,interfaces.ip,groups[].name
		jq -c '.result[] | {host: .host, ip: .interfaces[].ip, groups: [.groups[].name]}' /tmp/$USER-zabbix-output-$$
	else
		cat /tmp/$USER-zabbix-output-$$ | jq
	fi
}
function query_for_data () {
	curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null > /tmp/$USER-zabbix-output-$$
}

create_json_file

query_for_data

output_info
