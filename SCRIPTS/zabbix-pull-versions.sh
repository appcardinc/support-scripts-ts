#!/bin/bash

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT

zabbix_user="zabbix-api-user"
zabbix_pass=$(echo zabbix-api-user | md5sum | head -c 25)

cat >/tmp/$USER-zabbix-creds-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "$zabbix_user",
        "password": "$zabbix_pass"
    },
    "id": 1
}
EOF

USER_AUTH_COOKIE=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-creds-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result')

cat >/tmp/$USER-zabbix-get-host-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.get",
    "params": {
        "output": ["host"],
        "selectGroups": "extend",
        "selectInterfaces": ["ip"]
    },
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF

function get_version () {
key=$1
cat >/tmp/$USER-zabbix-get-host-version-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "item.get",
    "params": {
        "output": "extend",
        "hostids": "$id",
        "search": {
            "key_": "$key"
        },
        "sortfield": "name"
    },
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF
}

curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -c '.result[] | {hostid: .hostid, host: .host, ip: .interfaces[].ip, groups: [.groups[].name]}' | grep cpe.greenpoints > /tmp/all-devs-$$

if ! [ -z $1 ]; then
	grep -i $1.cpe.greenpoints.com /tmp/all-devs-$$ > /tmp/temp-$$
	mv /tmp/temp-$$ /tmp/all-devs-$$
fi

while read LINE; do
	id=$(echo $LINE | jq -r '.hostid')
	host=$(echo $LINE | jq -r '.host')

	get_version rpm.check.sh.jbrain.server
	version=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-version-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result[] | .lastvalue')
	if [ -z $version ]; then
		get_version jbrain.ng.code.version
		version=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-version-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result[] | .lastvalue')
	fi

	get_version eth0.mac
	mac=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-version-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result[] | .lastvalue')

	get_version vm.memory.size[total]
	memtotal_bytes=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-version-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result[] | .lastvalue')
	memtotal=$(printf %.3f "$(($memtotal_bytes/1000000))e-3")

	get_version system.sw.os
	kernel=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-host-version-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result[] | .lastvalue' | awk '{print $3}')

	echo "$host,$version,$mac,$kernel,$memtotal GB"
	unset -v host id key version mac memtotal memtotal_bytes kernel
done < /tmp/all-devs-$$
