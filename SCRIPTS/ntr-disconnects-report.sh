#!/bin/bash 

# script to get all ntrs restarts by branch then using the device IDs to check AWS for Firmware/Version

# output is recorded here:
# https://docs.google.com/spreadsheets/d/1y8di_OxCnnAaD4hjxhNz2mri5K0FlP7AbLuFyADCH-Q/edit#gid=93050678

export PGHOST="prod-rtdb.c40ku8gr3zsv.us-east-1.rds.amazonaws.com"
export PGPORT="5432"
export PGDATABASE="AppCardDB"
export PGUSER="ntrsearch"
export PGPASSWORD="e9e49a5801ef7b16cfe05921e"
export AWS_CONFIG_FILE=/home/support/.aws/config
export AWS_SHARED_CREDENTIALS_FILE=/home/support/.aws/credentials

trap 'rm -f /tmp/*-$$' EXIT

# get ntr restarts by branch
cat >/tmp/get-ntr-details-$$ <<-EOF
-- restarts by device
select left (m.name, 15) as "Merchant",
       m.merchant_id,
-- note that we are removing any literal commas that may be found on the branch_name
       left (REPLACE(mb.name, ',', ''), 15) as "Branch",
       mb.branch_id,
       a.device_name,
       a.device_id,
       mb.program_type,
       a.count total_restarts
from (select count (*) count, d.branch_id, d.device_id, d.device_name
    from device_presence dp, devices d, merchant_branches mb
    where dp.device_id = d.device_id
    and mb.branch_id = d.branch_id
    and dp.create_tstamp > now() - interval '1 week'
    and event_type = 'disconnected'
    and d.status != 3
and d.setup_type = 'PT7003-Terminal'
    group by 2, 3, 4) a, merchant_branches mb, merchants m
where a.branch_id = mb.branch_id and mb.merchant_id = m.merchant_id
  and a.count > 14
-- and device_id in (select device_id from device_disconnections_reasons where reason = 'DUPLICATE_CLIENTID')
order by 8 desc;
EOF

# run the sql cmd on postgress; remove the summary line; 
cat /tmp/get-ntr-details-$$ | psql -AF ',' | grep -vE 'rows\)|row\)' > /tmp/sql-output-$$

cut -d, -f6 /tmp/sql-output-$$ | grep -v device_id > /tmp/ntr-device-ids-$$


## start of python code and output to file
python - <<EOF > /tmp/ntr-device-data-$$
import boto3
import json

AWS_REGION = 'us-east-1'

def load_aws_credentials():
    session = boto3.Session()
    credentials = session.get_credentials()
    return credentials


credentials = load_aws_credentials()
client = boto3.client('iot-data', region_name=AWS_REGION, aws_access_key_id=credentials.access_key,
                      aws_secret_access_key=credentials.secret_key, aws_session_token=credentials.token)


with open("/tmp/ntr-device-ids-$$", "r") as input_file:
  for line in input_file:
    device = line.strip()
    data = json.loads(client.get_thing_shadow(thingName=str(device))['payload'].read())
    print(data['state']['reported']['current']['version'] + ',' + data['state']['reported']['current']['firmware'])
EOF

# add the header and add a comman at the beginning
echo -e "NTR version,FW version\n$(cat /tmp/ntr-device-data-$$)" | sed 's/^/,/g' > /tmp/ntr-final-$$

# print both csv formated data side by side and remove any <TAB>
pr -m -t -s /tmp/sql-output-$$ /tmp/ntr-final-$$ | sed 's/\t//g'
