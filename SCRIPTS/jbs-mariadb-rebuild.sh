#!/bin/bash

# purpose is to fix the mariadb corrupt issue.

# in a nutshell, this script will:
# - backup config files
# - uninstall jbrain pkgs
# - uninstall mariadb
# - reinstall old pkgs
# - restart services

#echo "under maintenance. exiting..."
#exit

shopt -s expand_aliases
alias sshr="/usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20'"
alias scpr="/usr/bin/scp -p -o StrictHostKeyChecking=no -o BatchMode=yes -o ConnectTimeout=10 -o ServerAliveInterval=3 "

trap 'rm -f /tmp/*-$$' EXIT

if [ "$#" -eq 0 ]; then
	echo Please add a JB as a parameter
	echo "$(basename $0) JB"
	echo 
	exit 1
fi

HOST=$1

####################################
echo "### creating /tmp/notok and stopping tomcat ###"
sshr $HOST "touch /tmp/notok ; systemctl stop tomcat"
echo -e "\n\n"

####################################
echo "### Confirming tomcat stopped ###"

if sshr $HOST "systemctl is-active tomcat" 2> /dev/null | grep -q "inactive"; then
	echo -e "### Tomcat on $HOST is stopped ###\n\n"
else
	echo "Looks like Tomcat did not stop. Cannot continue."
	echo "Exiting..."
	exit 1
fi


####################################
echo "### Backing up files in /opt/shsolutions/conf ###"
sshr $HOST "tar -Pcvzf /tmp/config-files.tar.gz /opt/shsolutions/conf/." 2> /dev/null
sshr $HOST "tar -Pczf /tmp/tomcat-logs.tar.gz /var/log/tomcat/." 2> /dev/null
echo -e "\n\n"


####################################
echo "### Getting a list of all jbrain pkgs installed ###"
sshr $HOST "rpm -qa --queryformat='%{VENDOR} %{NAME}-%{VERSION}-%{RELEASE}\n' | grep 'S&H Solutions' | awk '{print \$NF}' | sort | tee /tmp/jbrain-pkgs-installed-before.txt" 2> /dev/null | tee /tmp/$HOST-jbrain-pkgs-before-$$
echo -e "\n\n"


####################################
echo "### Removing all installed jbrain pkgs ###"
sshr $HOST "rpm -e --nodeps \$(cat /tmp/jbrain-pkgs-installed-before.txt) ; rpm -e ntr-server --noscripts" 2> /dev/null
echo -e "\n\n"


####################################
echo "### Confirming uninstalled jbrain pkgs ###"
sshr $HOST "rpm -qa --queryformat='%{VENDOR} %{NAME}-%{VERSION}-%{RELEASE}\n' | grep 'S&H Solutions' | awk '{print \$NF}'" 2> /dev/null > /tmp/$HOST-pkgs-left.txt-$$
if [ $(cat /tmp/$HOST-pkgs-left.txt-$$ | wc -l) -eq 0 ]; then
	echo -e "Did not find any pkgs.\n\n"
else
	echo -e "Looks like something did not get removed. Cannot continue."
	cat /tmp/$HOST-pkgs-left.txt-$$
	echo "exiting..."
	exit 1
fi


####################################
echo "### Stopping mariadb and removing it ###"
sshr $HOST "systemctl stop mariadb; rm -rf /var/lib/mysql; yum remove -y mariadb-server"
echo -e "\n\n"


####################################
echo "### RE-installing mariadb and starting service ###"
sshr $HOST "yum install -y mariadb-server; systemctl enable --now mariadb"
echo -e "\n\n"


####################################
echo "### Removing /opt/shsolutions directory ###"
sshr $HOST "rm -rf /opt/shsolutions"
echo -e "\n\n"


####################################
echo "### Installing tomcat jbrain pkg ###"
sshr $HOST "yum -y --disablerepo=* --enablerepo=jbrain install \$(grep tomcat /tmp/jbrain-pkgs-installed-before.txt); systemctl stop tomcat" 2> /dev/null
echo -e "\n\n"


####################################
echo "### Adding /etc/profile.d/jbrain.sh ###"
cat >/tmp/jbrain-sh-$$ <<EOF
export JRE_HOME=/usr/java/jre
export JAVA_HOME=/usr/java/jre
export SHSOLUTIONS_ENV=prod
SHSOLUTIONS_LOCALE=US
export SHSOLUTIONS_LOCALE=US
EOF
scpr /tmp/jbrain-sh-$$ root@$HOST:/etc/profile.d/jbrain.sh
echo -e "\n\n"


####################################
echo "### RE-installing previously installed jbrain pkgs ###"
sshr $HOST "yum -y --disablerepo=* --enablerepo=jbrain install \$(grep sh-jbrain-server /tmp/jbrain-pkgs-installed-before.txt)" 2> /dev/null
echo -e "\n\n"


####################################
echo "### 2nd attempt to RE-install missing jbrain pkgs ###"
sshr $HOST "for i in \$(cat /tmp/jbrain-pkgs-installed-before.txt ); do yum -y --disablerepo=* --enablerepo=jbrain install \$i; done" 2> /dev/null
echo -e "\n\n"


####################################
echo "### Confirming re-installed old jbrain pkgs ###"
sshr $HOST "rpm -qa --queryformat='%{VENDOR} %{NAME}-%{VERSION}-%{RELEASE}\n' | grep 'S&H Solutions' | awk '{print \$NF}' | sort | tee /tmp/jbrain-pkgs-installed-before.txt" 2> /dev/null > /tmp/$HOST-jbrain-pkgs-after-$$
if diff /tmp/$HOST-jbrain-pkgs-before-$$ /tmp/$HOST-jbrain-pkgs-after-$$ &> /dev/null; then
	echo -e "### Looks like all old pkg versions were installed. ###"
else
	if [ $(cat /tmp/$HOST-jbrain-pkgs-before-$$ | wc -l) -eq $(cat /tmp/$HOST-jbrain-pkgs-after-$$ | wc -l) ]; then
		echo "Some pkgs versions are different."
	else
		echo "Looks like some pkgs were not installed. You can install those manually."
	fi
	echo -e "Please see before and after:\n"
	sleep 5
	diff --suppress-common-lines -y /tmp/$HOST-jbrain-pkgs-before-$$ /tmp/$HOST-jbrain-pkgs-after-$$
	echo -e "\nWill continue..."
	sleep 5
fi
echo -e "\n\n"


####################################
echo "### Changing ownership of /opt/shsolutions/data/* ###"
sshr $HOST "chown tomcat:tomcat -R /opt/shsolutions/data/*" 2> /dev/null
echo -e "\n\n"


####################################
echo "### Restoring old config files into /opt/shsolutions/conf/ ###"
sshr $HOST "tar -Pxf /tmp/config-files.tar.gz" 2> /dev/null
sshr $HOST "tar -Pxf /tmp/tomcat-logs.tar.gz" 2> /dev/null
echo -e "\n\n"


####################################
echo "### Creating /opt/tomcat/conf/tomcat-users.xml file ###"
cat >/tmp/tomcat-user-xml-$$ <<EOF
<?xml version='1.0' encoding='utf-8'?>
<tomcat-users>
  <role rolename="manager"/>
  <role rolename="tomcat"/>
  <role rolename="admin"/>
  <role rolename="developer"/>
  <role rolename="enginewebservices"/>
  <role rolename="tomcatwebservices"/>
  <role rolename="superuser"/>
  <role rolename="operator"/>
  <user username="tomcat" password="1ampurej@v@" roles="manager,tomcat"/>
  <user username="admin" password="@wwnu+z!" roles="admin"/>
  <user username="developer" password="+hecr3at0rs" roles="developer"/>
  <user username="enginewebservices" password="enginewebservices" roles="enginewebservices"/>
  <user username="tomcatwebservices" password="l1nuxs1mpl3" roles="tomcatwebservices,manager"/>
  <user username="superuser" password="us3r0fst33l" roles="superuser"/>
  <user username="operator" password="9llconn3ct" roles="operator"/>
</tomcat-users>
EOF
scpr /tmp/tomcat-user-xml-$$ root@$HOST:/opt/tomcat/conf/tomcat-users.xml
echo -e "\n\n"

####################################
echo "### Restarting mariadb ###"
sshr $HOST "systemctl restart mariadb" 2> /dev/null
echo -e "\n\n"
sleep 5


####################################
echo "### Restarting tomcat ###"
sshr $HOST "systemctl restart tomcat" 2> /dev/null
echo -e "\n\n"


echo "####################################"
echo "Please check if all is well"
echo DONE
echo "####################################"
