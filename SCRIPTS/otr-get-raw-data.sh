#!/bin/bash 

# output raw data of X otr device
# then convert it to hex

trap 'rm -f /tmp/*-$$' EXIT

# validate user input
if [ "$#" -eq 0 ]; then
	echo -e "USAGE:"
	echo "./script deviceID"
	exit 1
fi

device_id=$1

cat >/tmp/get-otr-data-$$ <<-EOF
select count(*),data 
from device_raw_data where 
device_id=$device_id
and id > (select max(device_raw_data_id) from device_raw_data_receipt_assoc where device_id=$device_id)
group by 2  order by 1 desc
;
EOF

export PGHOST="prod-rtdb.c40ku8gr3zsv.us-east-1.rds.amazonaws.com"
export PGPORT="5432"
export PGDATABASE="AppCardDB"
export PGUSER="ntrsearch"
export PGPASSWORD="e9e49a5801ef7b16cfe05921e"

cat /tmp/get-otr-data-$$ | psql -t -AF ',' > /tmp/raw-data-$$

for i in $(cat /tmp/raw-data-$$ | cut -d, -f2 | cut -d'\' -f2 | sed 's/^x//g'); do
sh_raw_data=$i
echo "#####################################"
echo "#####################################"
python3 - <<EOF
import sys
import zlib

raw_data = '$sh_raw_data'
text=str(raw_data)
b = bytearray.fromhex(text)
raw = zlib.decompress(b)
print(raw_data)
print(raw)
EOF
done > /tmp/raw-to-hex-$$


echo "################# Below is the RAW data, taken from the db ########################"
cat /tmp/raw-data-$$
echo
echo "################# Converted raw data to hex ######################"
cat /tmp/raw-to-hex-$$ | column -t
