#!/bin/bash

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT


# print help msg when run w/o parameters
if [ "$#" -eq 0 ]; then
	echo USAGE:
	echo "./script jbname"
	echo
	exit 1
fi

# change 1st parameter input to uppercase
JB_NAME=$(echo $1 | tr '[:upper:]' '[:lower:]' | cut -d. -f1)

zabbix_user="zabbix-api-user"
zabbix_pass=$(echo zabbix-api-user | md5sum | head -c 25)


if host $JB_NAME | grep -iq "has address" ; then
	JB_IP=$(host $JB_NAME | grep -i address | awk '{print $NF}')
	if host $JB_NAME | grep -q "cpe.greenpoints.com"; then # then regular JB
		if /usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $JB_NAME "hostname" 2> /dev/null | grep -q cpe; then
			JB_FQDN=$(/usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $JB_NAME "hostname" 2> /dev/null)
		fi
		#JB_FQDN=$(echo $JB_NAME | sed 's/$/.cpe.greenpoints.com/g')
		ZBX_GROUP_ID=17 # Jbrain's OLD image Centos 7
	elif host $JB_NAME | grep -q "pl.appcard.com"; then # then super JB
		if /usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $JB_NAME "hostname" 2> /dev/null | grep -q pl.appcard.com; then
			JB_FQDN=$(/usr/bin/ssh -l root -o StrictHostKeyChecking=no -o 'ServerAliveInterval 20' $JB_NAME "hostname" 2> /dev/null)
		fi
		JB_FQDN=$(echo $JB_NAME | sed 's/$/.appcard.com/g')
		ZBX_GROUP_ID=246 # Super Jbrain Centos 7
	else
		echo "Not sure what type of JB this is...either regular or SuperJB"
		exit 1
	fi
else
	echo Looks like $JB_NAME is not in dns. Please add
	exit 1
fi

cat >/tmp/$USER-zabbix-creds-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "$zabbix_user",
        "password": "$zabbix_pass"
    },
    "id": 1
}
EOF

USER_AUTH_COOKIE=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-creds-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result')

echo "Please type the branch ID of this JB"
echo "If this is for ACE, type 0000 as the branch ID"
read -p "ID: " brnid

/home/support/SCRIPTS/zabbix-pull-groups.sh | grep "$brnid - " > /tmp/brnids-$$
echo

# if file has data, then something found
if [ -s /tmp/brnids-$$ ]; then
	# if result is one line
	if [ $(cat /tmp/brnids-$$ | wc -l) -eq 1 ]; then
		cat -n /tmp/brnids-$$; echo
		read -p "Found above branch, ok to add? (y/n) " yn
		case $yn in
			[Yy])	ZBX_BRN_ID=$(cat /tmp/brnids-$$ | jq -r '.groupid');;
			[Nn])	echo "Exiting, nothing to do"; exit 1;;
			*)	echo "Type Y or N... exiting..."; exit 1;;
		esac
	else # else results are multi line
		# possible ACE?
		if cat -n /tmp/brnids-$$ | grep -q "0000 - ACE"; then
			cat -n /tmp/brnids-$$; echo
			echo "Looks like you are looking for an ACE store."
			read -p "Please type the store number instead: " strid
			/home/support/SCRIPTS/zabbix-pull-groups.sh | grep "$strid" > /tmp/brnids-$$
			if [ $(cat /tmp/brnids-$$ | wc -l) -eq 1 ]; then
				cat -n /tmp/brnids-$$; echo
				read -p "Found above branch, ok to add? (y/n) " yn
				case $yn in
					[Yy])	ZBX_BRN_ID=$(cat /tmp/brnids-$$ | jq -r '.groupid');;
					[Nn])	echo "Exiting, nothing to do"; exit 1;;
					*)	echo "Type Y or N... exiting..."; exit 1;;
				esac
			else
				cat -n /tmp/brnids-$$; echo
				echo -e "Either the ID is invalid or not found in zabbix.\nHave admin add the group(s)"
				exit 1
			fi
		fi
	fi
else # else nothing found...
	echo -e "Either branch ID is invalid or not found in zabbix.\nHave admin add the group(s)"
	exit 1
fi

cat >/tmp/$USER-zabbix-create-host-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "host.create",
    "params": {
        "host": "$JB_FQDN",
        "interfaces": [
            {
                "dns": "$JB_FQDN",
                "ip": "$JB_IP",
                "main": "1",
                "port": "10050",
                "type": "1",
                "useip": "1"
            }
        ],
        "groups": [
            {
                "groupid": "5",
                "internal": "1"
            },
            {
                "groupid": "$ZBX_GROUP_ID"
            },
            {
                "groupid": "$ZBX_BRN_ID"
            }
        ],
        "templates": [
            {
                "templateid": "10331"
            },
            {
                "templateid": "13891"
	    }
        ]
    },
    "id": 2,
    "auth": "$USER_AUTH_COOKIE"
}
EOF

curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-create-host-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null
