#!/bin/bash

trap 'rm -f /tmp/*-$$ 2> /dev/null' EXIT


cat >/tmp/$USER-zabbix-creds-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": "zabbix-api-user",
        "password": "fe36fae25dcd3af489654bb41"
    },
    "id": 1
}
EOF

USER_AUTH_COOKIE=$(curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-creds-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null | jq -r '.result')


cat >/tmp/$USER-zabbix-get-group-$$ <<EOF
{
    "jsonrpc": "2.0",
    "method": "hostgroup.get",
    "params": {
        "output": "extend"
    },
    "auth": "$USER_AUTH_COOKIE",
    "id": 1
}
EOF

curl -k -X POST -H 'Content-Type:application/json-rpc' -H 'Accept: application/json' --data-binary @/tmp/$USER-zabbix-get-group-$$ http://prod-zabbix2.appcard.com:8999/api_jsonrpc.php -v -s 2> /dev/null > /tmp/$USER-zabbix-output-$$

# get only two fields: name
jq -c '.result[] | {groupid: .groupid, name: .name}' /tmp/$USER-zabbix-output-$$
